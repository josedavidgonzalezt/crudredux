import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    COMENZAR_DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_EXITO,
    DESCARGA_PRODUCTOS_ERROR,
    OBTENER_PRODUCTO_ELIMINAR,
    PRODUCTO_ELIMINADO_EXITO,
    PRODUCTO_ELIMINADO_ERROR,
    OBTENER_PRODUCTO_EDITAR,
    COMENZAR_EDICION_PRODUCTO,
    PRODUCTO_EDITADO_EXITO,
    PRODUCTO_EDITADO_ERROR,
} from '../types';
import clienteAxios from '../config/axios';
import swal from 'sweetalert2';
import Swal from 'sweetalert2';


//crear nuevos productos

export function crearNuevoProductoAction(producto){
    return async (dispatch)=>{
        dispatch( agregarProducto() );
        try {
            //insertr en la API
            await clienteAxios.post('/productos', producto);

            //actualiza el state
            dispatch( agregarProductoExito(producto) );
            
            //alerta
            swal.fire(
                'Correcto',
                'El Producto se agregó correctamente',
                'success'
            )
        } catch (error) {
            console.log(error)
            dispatch( agregarProductoError(true) );

            //alerta de error
            Swal.fire({
                icon:'error',
                title:'Hubo un error',
                text:"Hubo un error, intenta de nuevo"
            })
        }
    }
}

const agregarProducto = () => ({
    type: AGREGAR_PRODUCTO
});

//si el producto se guarda en la base de datos
const agregarProductoExito = producto  => ({
    type: AGREGAR_PRODUCTO_EXITO,
    payload: producto
});

//si hubo un errror
const agregarProductoError = estado => ({
    type: AGREGAR_PRODUCTO_ERROR,
    paylodad: estado 
});

//funcion que descarga los productos de la base de datos
export function obtenerProductoAction(){
    return async (dispatch)=>{
        dispatch(descargaProductos())
        try {
            const respueta = await clienteAxios.get('/productos');
            dispatch(descargaProductosExito(respueta.data));

        } catch (error) {
            console.log(error)
            dispatch(descargaProductosError());
            
        }
    }
}

const descargaProductos=()=>({
    type: COMENZAR_DESCARGA_PRODUCTOS,
    payload: true
});

const descargaProductosExito=(productos)=>({
    type: DESCARGA_PRODUCTOS_EXITO,
    payload: productos
});

const descargaProductosError=()=>({
    type: DESCARGA_PRODUCTOS_ERROR,
    payload: true
});

//selecciona el producto a eliminar

export function borrarProductoAction(id){
    return async (dispatch)=>{
        dispatch( obtenerProductoEliminar(id));
        try {
            await clienteAxios.delete(`/productos/${id}`);
            dispatch( eliminarProductoExio(id));
            Swal.fire(
                'Eliminado!',
                'El producto ha sido eliminado.',
                'success'
              )
        } catch (error) {
            console.log(error);
            dispatch( eliminarProductoError());
        }
    }
}

const obtenerProductoEliminar=id=>({
    type: OBTENER_PRODUCTO_ELIMINAR,
    payload: id
});
const eliminarProductoExio=()=>({
    type: PRODUCTO_ELIMINADO_EXITO
});
const eliminarProductoError=()=>({
    type: PRODUCTO_ELIMINADO_ERROR,
    payload:true
});

//Colocar producto en edición
export function obtenerProductoEditar(producto){
    return (dispatch)=>{
        dispatch(obtenerProductoEditarAction(producto))
    }
}

const obtenerProductoEditarAction= producto =>({
    type:OBTENER_PRODUCTO_EDITAR,
    payload: producto
});

//edita un registro en la api y state

export function editarProductoAction(producto){
    return async(dispatch)=>{
        dispatch(editarProduto());
        try {
            clienteAxios.put(`/productos/${producto.id}`, producto);
            dispatch( editarProductoExito(producto) )
        } catch (error) {
            console.log(error);
            dispatch( editarProductoError() )
        }
    }
}

const editarProduto=()=>({
    type: COMENZAR_EDICION_PRODUCTO,
});

const editarProductoExito = producto =>({
    type: PRODUCTO_EDITADO_EXITO,
    payload: producto
});

const editarProductoError =() =>({
    type: PRODUCTO_EDITADO_ERROR,
    payload: true
});