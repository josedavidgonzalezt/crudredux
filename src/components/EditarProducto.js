import {useState, useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { editarProductoAction } from '../actions/productoActions';
import {useHistory} from 'react-router-dom';


const EditarProducto = () => {
    //state de producto editar
    const [producto, guardarProducto] = useState({
        nombre:"",
        precio :""
    });
    const {nombre, precio} =producto;
    const history = useHistory();
    //producto a editar
    const productoeditar = useSelector(state => state.productos.productoeditar);
    //inicializa el dispatch
    const dispatch = useDispatch();

    //Llenar el state con useEffect
    useEffect(()=>{
        if(productoeditar){
            guardarProducto(productoeditar)
        }else{
            history.push('/')
        }
        //eslint-disable-next-line
    },[])

    //leer los datos del formulario
    const handleChange=e=>{
        guardarProducto({
            ...producto,
            [e.target.name] : e.target.value
        })
    }

    const submitEditarProducto =e=>{
        e.preventDefault();
        dispatch( editarProductoAction(producto) );
        history.push('/')
    }

    return ( 
        <div className="row justify-content-center">
           <div className="col-md-8">
               <div className="card">
                   <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Editar Producto
                        </h2>
                        <form
                            onSubmit={submitEditarProducto}
                        >
                            <div className="form-group">
                                <label>Nombre Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre Producto"
                                    name="nombre"
                                    value={nombre}
                                    onChange={handleChange}
                                />
                                <label>Precio Producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Precio Producto"
                                    name="precio"
                                    value={precio}
                                    onChange={handleChange}
                                />
                            </div>
                            <button
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase"
                            >
                                Guardar Cambios
                            </button>
                        </form>
                   </div>
               </div>
           </div>
       </div>
     );
}
 
export default EditarProducto;