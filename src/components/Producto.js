import { useHistory } from "react-router-dom";

//redux
import { borrarProductoAction, obtenerProductoEditar } from "../actions/productoActions";
import { useDispatch } from "react-redux";
import Swal from "sweetalert2";
const Producto = ({producto}) => {
    const {nombre, precio, id} = producto;

    const dispatch = useDispatch();

    const confirmarEliminarProducto = id =>{
        //preguntar al usuario
        Swal.fire({
            title: '¿Estas seguro?',
            text: "¡No se puede deshacer la siguiente acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.isConfirmed) {
                //pasar al action
                dispatch(borrarProductoAction(id));
            }
          })
    }

    //habilitar history
    const history = useHistory();
    //funcion que redirige de forma programada
    const redireccionarEdicion= producto =>{
        dispatch(obtenerProductoEditar(producto))
        history.push(`/productos/editar/${id}`);
    }
    

    return ( 
        <tr>
            <td>{nombre}</td>
            <td><span className="font-weigth-bold">${precio}</span></td>
            <td className="acciones">
                <button 
                    to={`/productos/editar/${id}`} 
                    className="btn btn-primary mr-2"
                    onClick={()=>redireccionarEdicion(producto)}    
                >
                    editar
                </button>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={()=>confirmarEliminarProducto(id)}
                >
                    Eliminar
                </button>
            </td>
        </tr>
     );
}
 
export default Producto;