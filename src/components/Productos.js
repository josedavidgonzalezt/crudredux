import { useEffect } from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { obtenerProductoAction } from '../actions/productoActions';
import Producto from './Producto';


const Productos = () => { 
    
    const dispatch = useDispatch();

    useEffect(()=>{
        //consultar la api
        const cargarProductos = () => dispatch(obtenerProductoAction());
        cargarProductos()
        //eslint-disable-next-line
    },[])

    const productos = useSelector(state=> state.productos.productos);
    const error = useSelector(state=> state.productos.error);
    const loading = useSelector(state=> state.productos.loading);

    return ( 
        <>
            <h2 className="text-center my-5">Listado Productos </h2>
            {error ? <p className="font-weight-bold alert alert-danger text-center mt-4">Hubo un errror</p> :null}
            {loading ? <p className="text-center mt-4">cargando</p> :null}
            <table className="table table-striped">
                <thead className="bg-primary table-dark">
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {productos.length===0 
                        ? <tr><td>No hay productos</td></tr>
                        : productos.map(producto=>(
                            <Producto
                                key={producto.id}
                                producto={producto}
                            />
                        ))}        
                </tbody>
            </table>
        </>
     );
}
 
export default Productos;